#!/usr/bin/python
# -*- coding: utf-8 -*-

from getpass import getpass
from json import loads
from pyzabbix import ZabbixAPI, ZabbixAPIException
from uuid import uuid4
from time import sleep

import ldap
import os
import argparse
import logging
import sys

LOGGER = None


class LDAPLogin(object):
    def __init__(self, config):
        self.server = config["ldap"]["address"]
        self.bind_dn = config["ldap"]["bind_dn"]
        self.base_dn = config["ldap"]["base_dn"]
        self.permitted_groups = config["ldap"]["permitted_groups"]
        self.user_data = dict()
        self.domain_password = os.getenv("DOMAIN_PASSWORD")
        if not self.domain_password:
            self.domain_password = getpass("Please enter domain user password for retrieve data. Password: ")

    def get_data(self):
        connection = ldap.initialize("ldap://{0}".format(self.server))
        connection.protocol_version = 3
        connection.set_option(ldap.OPT_REFERRALS, 0)
        connection.simple_bind_s("{0}".format(self.bind_dn), self.domain_password)
        attributes = ["mail", "samaccountname", "name", "sn"]
        for k, v in self.permitted_groups.iteritems():
            LOGGER.info("Searching for '{0}'. Filter: '{1}'".format(v, k))
            ldap_filter = "(memberOf={0})".format(k)
            self.user_data[v] = [i for i in connection.search_s(self.base_dn, ldap.SCOPE_SUBTREE, ldap_filter, attributes) if i[0] is not None]
            LOGGER.debug("Found items for '{0}'. Items: '{1}'".format(v, self.user_data[v]))
        return self.user_data


class ZabbixController(object):
    def __init__(self, config, user_data, script_config):
        self.user_data = user_data
        self.script_config = script_config
        try:
            self.z = ZabbixAPI(url=config.zabbix_url)
            self.z.session.verify = False
        except ZabbixAPIException as e:
            LOGGER.error("Cannot login Zabbix API. Exception: '{0}'.".format(e))
            sys.exit(-1)

    def is_group_exists(self, group_name):
        for i in self.z.usergroup.get(status=0, output="extend"):
            if i["name"] == group_name:
                return i["usrgrpid"]

    def is_user_exists(self, user_alias):
        for i in self.z.user.get(output="extend"):
            if i["alias"] == user_alias:
                return i["userid"]

    def get_media_type_id(self, media_name):
        try:
            return self.z.mediatype.get(filter={"description": media_name})[0]["mediatypeid"]
        except IndexError:
            LOGGER.error("This media type is unknown. Media type: '{0}'.".format(media_name))
            sys.exit(-1)

    def get_group_users(self, group_id):
        return [i["alias"] for i in self.z.user.get(output="extend", usrgrpids=group_id)]

    def get_host_group_id(self, hostgroup_name):
        return self.z.hostgroup.get(output="extend", filter={"name": [hostgroup_name]})[0]["groupid"]

    @staticmethod
    def get_media_severity(severity_string):
        severity = ""
        for _ in severity_string.strip().split(","):
            severity += "1"
        return str(int(severity, 2))

    @staticmethod
    def get_missing_users_in_group(group_users, user_data):
        return set(["".join(v["sAMAccountName"]).lower() for _, v in user_data]) - set(group_users)

    @staticmethod
    def get_extra_users_in_group(group_users, user_data):
        return set(group_users) - set(["".join(v["sAMAccountName"]).lower() for _, v in user_data])

    def update_user_media(self, media_name, user_info, user_z_id):
        media_type_id = self.get_media_type_id(media_name=self.script_config["medias"][media_name]["description"])
        # if media_type_id not in self.z.user.get(output="extend", userids=user_z_id, selectMedias=["mediatypeid"])[0]["medias"][0].values():
        media_defaults = {
            "mediatypeid": media_type_id,
            "sendto": user_info[self.script_config["medias"][media_name]["ad_key"]].lower(),
            "active": "0",
            "severity": self.get_media_severity(severity_string=self.script_config["medias"][media_name]["severities"]),
            "period": "1-7,00:00-24:00"
        }
        LOGGER.info("Media type added to '{0}'. Media: '{1}'.".format(user_info["sAMAccountName"].lower(), media_name))
        LOGGER.debug("Media type added to '{0}'. Defaults: '{1}'.".format(user_info["sAMAccountName"].lower(), media_defaults))
        return self.z.user.update(userid=user_z_id, user_medias=[media_defaults])

    def create_user(self, user_info, z_group_id):
        user_defaults = {
            "alias": user_info["sAMAccountName"].lower(),
            "name": user_info["name"].replace(user_info["sn"], "").strip(),
            "surname": user_info["sn"],
            "autologin": 0,
            "type": 1,
            "usrgrps": [{"usrgrpid": str(z_group_id)}],
            "passwd": str(uuid4()).split("-")[-1]
        }
        LOGGER.info("User created. Username: '{0}'.".format(user_defaults["alias"]))
        sleep(1)
        return self.z.user.create(user_defaults)

    def create_group(self, group_name, perms):
        LOGGER.info("Group created. Group name: '{0}'.".format(group_name))
        rights = list()
        perm_converter = {
            "access-denied": 0,
            "read-only": 2,
            "read-write": 3
        }
        for r in perms:
            rights.append({
                "permission": perm_converter[r[1]],
                "id": self.get_host_group_id(hostgroup_name=r[0])
            })
        sleep(1)
        return self.z.usergroup.create(name=group_name, rights=rights)

    def start_process(self):
        for k, v in self.user_data.iteritems():
            group_id = self.is_group_exists(group_name=k)
            if not group_id:
                LOGGER.info("'{0}' named group not found. Will be create.".format(k))
                self.create_group(group_name=k, perms=self.script_config["zabbix_attribute"][k]["perms"])
                group_id = self.is_group_exists(group_name=k)
            for _, user_data in v:
                if "sn" not in user_data:
                    continue
                user_data = {k: "".join(v) for k, v in user_data.iteritems()}
                if not self.is_user_exists(user_alias=user_data["sAMAccountName"].lower()):
                    self.create_user(user_info=user_data, z_group_id=group_id)
                for media_name in self.script_config["zabbix_attribute"][k]["enabled_medias"]:
                    self.update_user_media(media_name=media_name, user_info=user_data, user_z_id=self.is_user_exists(user_alias=user_data["sAMAccountName"].lower()))
            for d_username in self.get_extra_users_in_group(group_users=self.get_group_users(group_id=group_id), user_data=v):
                LOGGER.info("User not in '{0}' named group. Will be delete. Username: '{1}'.".format(k, d_username))
                self.z.user.delete(self.is_user_exists(user_alias=d_username))


def must_environ_control():
    if not os.getenv("ZABBIX_USER") or not os.getenv("ZABBIX_PASSWORD"):
        LOGGER.error("You have to set 'ZABBIX_USER' and 'ZABBIX_PASSWORD' variables.")
        sys.exit(-1)


def arg_parser():
    parser = argparse.ArgumentParser(description="Zabbix Active Directory Sync")
    parser.add_argument("-f", dest="config_file", action="store", required=True, help="Config file path.")
    parser.add_argument("-u", dest="zabbix_url", action="store", required=True, help="Zabbix URL.(Ex. http://127.0.0.1)")
    parser.add_argument("-v", dest="verbose", action="store_true", default=False, help="Verbose log.")
    return parser.parse_args()


def configure_logger(config):
    global LOGGER
    LOGGER = logging.getLogger("Sync Operator")
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s \"%(message)s\"')
    if config.verbose:
        logging.getLogger().setLevel(logging.DEBUG)


def parse_config_file(file_path):
    if not os.path.exists(file_path):
        LOGGER.error("Config file does not exist. Path: {0}".format(file_path))
        sys.exit(-1)
    with open(file_path, "r") as c_file:
        return loads(c_file.read())


if __name__ == "__main__":
    args = arg_parser()
    configure_logger(config=args)
    must_environ_control()
    sync_config = parse_config_file(args.config_file)
    l = LDAPLogin(config=sync_config)
    z = ZabbixController(config=args, user_data=l.get_data(), script_config=sync_config)
    z.start_process()
