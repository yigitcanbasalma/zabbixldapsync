### Yapısı ve Çalışma Prensibi
Scriptin amacı, LDAP entegrasyonu yapılmış olan Zabbix uygulaması içerisindeki kullanıcıları güncel tutmak, bunun yanı sıra, insan operasyonuna gerek bırakmadan kullanıcıları, grupları ve kullanıcı haklarını güncel tutmaktır.

### Konfigürasyon Dosya Yapısı
Konfigürasyon dosyasını üç ana yapıda inceleyebiliriz.

#####1. LDAP Server Konfigürasyonu
```
"ldap": {
    "address": "<ldap_address>",
    "bind_dn": "<ldap_user>@<your_domain>",
    "base_dn": "<dn_identifier>",
    "permitted_groups": {
         "<full_cn_for_group>": "<zabbix_group_name>"
    }
}
```
    
#####2. Medya Tipi Konfigürasyonu
```
"medias": {
    "email": {
         "description": "Email",
         "severities": "Disaster,High,Average,Warning,Information,Not Classified",
         "ad_key": "mail"
    }
}
```
Bu alanda yapılan tanım, zabbix üzerinde yer alan hangi medya tipleri kullanılacak ve  LDAP üzerinden alınan erişim bilgileri, (telefon numarası, e-mail adresi vb.) hangi medyalar için argüman olarak kullanıcak sorularına cevap vermektedir. **ÖNEMLİ**: "ad_key" parametresi, LDAP üzerinden dönen dictionary içerisinde, medya için gerekli bilgiyi barındıran anahtarı tanımlamaktadır.
    
#####3. Zabbix Konfigürasyonu
```
"zabbix_attribute": {
    "<zabbix_group_name>": {
        "perms": [
            ["<server_group_name_for_permission>", "<permission_name(read-only, read-write, access-denied)>"]
        ],
        "enabled_medias": [
            "email"
        ]
    }
}
```
"enabled_medias" parametresini kullanarak, farklı gruplara, farklı medya tanımları da yapabilirsiniz.
    
### Kullanımı
Aşağıda yer alan örnek kullanım size yardımcı olacaktır. __ÖNEMLİ__: Bu script, python 2.x versiyonu ile denenmiştir. 3.x için düzeltmeler gerekebilir.
```bash
export ZABBIX_USER="admin"
export ZABBIX_PASSWORD="zabbix"
python zabbix_sync.py -f /path/to/sync_conf.py -u http://zabbix.local
```